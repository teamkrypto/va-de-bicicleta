package echo;

import static org.mockito.Mockito.*;
import main.util.JavalinApp;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import cartaodecredito.CartaoDeCredito;
import cartaodecredito.CartaoDeCreditoController;
import cartaodecredito.CartaoDeCreditoForm;
import cobranca.CieloIntegracao;
import cobranca.Cobranca;
import cobranca.CobrancaDAO;
import cobranca.CobrancaForm;
import cobranca.ExpedicaoCobranca;
import cobranca.FilaCobranca;
import cobranca.Pagamento;
import cobranca.Status;
import email.Email;
import email.EmailForm;
import erro.Erro;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.JsonNode;

class ControllerTest {
	private static JavalinApp app = new JavalinApp();

	@BeforeAll
	static void init() {
		app.start(7010);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}

	@Test
	void getCobrancaByIdTest() {
		HttpResponse<String> response = Unirest
				.get("http://localhost:7010/cobranca/3fa85f64-5717-4562-b3fc-2c963f66afa6").asString();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getIdCobranca() {
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		String id = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
		assertEquals(id, cobranca.getId());
	}

	@Test
	void getValorCobranca() {
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", 10);
		cobranca.setValor(15);
		int valor = cobranca.getValor();
		assertEquals(15, valor);
	}

	@Test
	void getCiclistaIdCobranca() {
		Cobranca cobranca = new Cobranca("a1b2", 10);
		cobranca.setCiclistaId("3fa85f64-5717-4562-b3fc-2c963f66afa6");
		String id = cobranca.getCiclistaId();
		assertEquals("3fa85f64-5717-4562-b3fc-2c963f66afa6", id);
	}

	@Test
	void getHoraSolicitacaoCobranca() {
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		cobranca.setHoraSolicitacao("2022-01-14T20:36:00.491Z");
		String horaSolicitacao = cobranca.getHoraSolicitacao();
		assertEquals("2022-01-14T20:36:00.491Z", horaSolicitacao);
	}

	@Test
	void getHoraFinalizacaoCobranca() {
		Cobranca cobranca = new Cobranca(Status.PAGA, "2022-01-14T20:36:00.491Z", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		cobranca.setHoraFinalizacao("2022-01-14T20:38:00.491Z");
		String horaFinalizacao = cobranca.getHoraFinalizacao();
		assertEquals("2022-01-14T20:38:00.491Z", horaFinalizacao);
	}

	@Test
	void getStatusCobranca() {
		Cobranca cobranca = new Cobranca(Status.PENDENTE, "2022-01-14T20:36:00.491Z", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		cobranca.setStatus(Status.PAGA);
		assertEquals(Status.PAGA, cobranca.getStatus());
	}

	@Test
	void cieloIntegracaoTest() {
		CieloIntegracao cielo = new CieloIntegracao("420550a6-f9db-4bd3-92fc-9fcb2558daed");
		String merchantOrderId = "420550a6-f9db-4bd3-92fc-9fcb2558daed";
		assertEquals(merchantOrderId, cielo.MerchantOrderId);
	}

	@Test
	void getValorPagamentoTest() {
		Pagamento payment = new Pagamento();
		payment.setAmount(10);
		assertEquals(10, payment.getAmount());
	}

	@Test
	void getTipoPagamentoTest() {
		Pagamento payment = new Pagamento();
		payment.setType("credito");
		String tipo = "credito";
		assertEquals(tipo, payment.getType());
	}

	@Test
	void getInstallmentsTest() {
		Pagamento payment = new Pagamento();
		payment.setInstallments(1);
		int installment = 1;
		assertEquals(installment, payment.getInstallments());
	}

	@Test
	void filaCobrancaTest() {
		FilaCobranca fc = new FilaCobranca();
		Queue<Cobranca> filaDeCobrancas = new LinkedList<>();
		Queue<Cobranca> filaCobrancas = new LinkedList<>();
		for (Cobranca c : filaCobrancas) {
			filaCobrancas.add(c);
		}
		FilaCobranca filaC = new FilaCobranca(filaDeCobrancas);
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");

		filaDeCobrancas.add(cobranca);
		filaC.setFilaCobranca(cobranca);
		assertEquals(filaDeCobrancas, filaC.getFilaCobranca());
	}

	@Test
	void cobrancaFormTest() {
		CobrancaForm cf = new CobrancaForm();
		CobrancaForm cobForm = new CobrancaForm("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		CobrancaForm cobrancaForm = new CobrancaForm("3fa85f64-5717-4562-b3fc-2c963f66afa6", 1);
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		Cobranca resultado = cobForm.desserializar();
		Cobranca cobrancaPost = cobForm.desserializarPostCobranca();

		assertEquals(cobranca.getId(), resultado.getId());
	}

	@Test
	void cobrancaDAOTest() {
		CobrancaDAO cd = new CobrancaDAO();
		ArrayList<Cobranca> cobrancas = new ArrayList<Cobranca>();
		cobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa7", 1));
		ArrayList<Cobranca> cobrancasNew = new ArrayList<Cobranca>();
		cobrancasNew = cd.getCobrancas();
		assertEquals(false, cobrancas.isEmpty());
	}

	@Test
	void erroTest() {
		Erro e = new Erro();
		e.setId("1");
		String id = e.getId();
		e.setCodigo("404");
		String codigo = e.getCodigo();
		e.setMensagem("Not found");
		String mensagem = e.getMensagem();
		assertEquals("Not found", mensagem);
	}

	@Test
	void emailFormTest() {
		EmailForm ef = new EmailForm();
		EmailForm email = new EmailForm("email@mail.com", "Teste");
		email.desserializar();
		assertEquals("Teste", email.mensagem);
	}

	@Test
	void cartaoDeCreditoFormTest() {
		CartaoDeCreditoForm cc = new CartaoDeCreditoForm();
		CartaoDeCreditoForm ccForm = new CartaoDeCreditoForm("Fulano Teste", "0000.0000.0000.0004", "12/2024", "321");
		ccForm.desserialize();
		assertEquals("321", ccForm.cvv);
	}

	@Test
	void cartaoDeCreditoTest() {
		CartaoDeCredito cc = new CartaoDeCredito("Fulano Teste", "0000.0000.0000.0004", "12/2024", "321");
		cc.setCvv("123");
		String cvv = cc.getCvv();
		cc.setNumero("0000.0000.0000.0001");
		String numero = cc.getNumero();
		cc.setNomeTitular("Fulano Tester");
		String nome = cc.getNomeTitular();
		cc.setValidade("12/2025");
		String validade = cc.getValidade();
		cc.setId("1");
		String id = cc.getId();
		assertEquals("1", id);
	}

	@Test
	void statusCanceladaTest() {
		Status pendente = Status.CANCELADA;
		boolean teste = false;

		if (pendente == Status.CANCELADA) {
			teste = true;
		}

		assertEquals(true, teste);
	}

	@Test
	void statusPendenteTest() {
		Status pendente = Status.PENDENTE;
		boolean teste = false;

		if (pendente == Status.PENDENTE) {
			teste = true;
		}

		assertEquals(true, teste);
	}

	@Test
	void statusOcupadaTest() {
		Status pendente = Status.OCUPADA;
		boolean teste = false;

		if (pendente == Status.OCUPADA) {
			teste = true;
		}

		assertEquals(true, teste);
	}

	@Test
	void statusFalhaTest() {
		Status pendente = Status.FALHA;
		boolean teste = false;

		if (pendente == Status.FALHA) {
			teste = true;
		}

		assertEquals(true, teste);
	}

	@Test
	void statusPagaTest() {
		Status pendente = Status.PAGA;
		boolean teste = false;

		if (pendente == Status.PAGA) {
			teste = true;
		}

		assertEquals(true, teste);
	}

	@Test
	void emailTest() {
		Email e = new Email("email@mail.com", "Teste");
		e.setId("1");
		String id = e.getId();
		e.setEmail("teste@mail.com");
		String email = e.getEmail();
		e.setMensagem("Teste teste");
		String mensagem = e.getMensagem();
		assertEquals("Teste teste", mensagem);
	}

	@Test
	void expedicaoCobrancaTest() {
		Cobranca cobranca = new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "", "", 10,
				"3fa85f64-5717-4562-b3fc-2c963f66afa6");
		CartaoDeCredito cartao = new CartaoDeCredito("Fulano Teste", "0000.0000.0000.0004", "12/2024", "321");
		ExpedicaoCobranca expc = new ExpedicaoCobranca();
		ExpedicaoCobranca expCob = new ExpedicaoCobranca(cobranca, cartao);
		expCob.setCobranca(cobranca);
		cobranca = expCob.getCobranca();
		assertEquals(10, cobranca.getValor());
	}

	// efetua cobranca
	@Test
	void postCobrancaTest() {
		HttpResponse<JsonNode> response = Unirest.post("https://teamkrypto-vadebicicleta.herokuapp.com/cobranca")
				.body("{\"ciclistaId\":\3fa85f64-5717-4562-b3fc-2c963f66afa6\", "
						+ "\"valor\":\"15\"}")
				.asJson();

		assertEquals(400, response.getStatus());
	}

	// @Test
	// void postSendEmailTest()
	// { // Teste de envio de e-mail
	// HttpResponse<JsonNode> response =
	// Unirest.post("https://va-de-bicicleta.herokuapp.com/enviarEmail").body("{\"email\":\"mthlucena@edu.unirio.br\",
	// \"mensagem\":\"Este é um teste de envio de e-mail.\"}").asJson();
	// assertEquals(200, response.getStatus());
	// }

	// envia uma cobranca para a fila de cobrancas
	@Test
	void postFilaCobrancaTest() {
		HttpResponse<JsonNode> response = Unirest.post("https://teamkrypto-vadebicicleta.herokuapp.com/filaCobranca")
				.body(
						"{\"status\":\"PAGA\", "
								+ "\"horaSolicitacao\":\"2022-01-14T20:36:00.491Z\", "
								+ "\"horaFinalizacao\":\"2022-01-14T20:38:00.491Z\", "
								+ "\"valor\":\"10\", "
								+ "\"ciclistaId\":\"000083d0-48d6-4751-9afa-76ec5d6fb0ee\"}")
				.asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postCheckCartaoDeCreditoTest() {
		HttpResponse<JsonNode> response = Unirest
				.post("https://teamkrypto-vadebicicleta.herokuapp.com/validaCartaoDeCredito")
				.body("{\"numero\":\"0000.0000.0000.0004\", \"validade\":\"12/2024\", \"nomeTitular\": \"Manuel Teste\", \"cvv\":\"321\"}")
				.asJson();

		assertEquals(200, response.getStatus());
	}

	// @Test
	void getCartaoDeCreditoIntegracaoTest() {
		HttpResponse response = Unirest
				.get("https://teamkrypto-va-de-bike-aluguel.herokuapp.com/cartaoDeCredito/3fa85f64-5717-4562-b3fc-2c963f66afa6")
				.asString();

		assertEquals(200, response.getStatus());
	}

}
