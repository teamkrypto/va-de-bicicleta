package email;
import java.util.UUID;

public class Email 
{
	private String id;
	private String destinatario;
	private String mensagem;

	public Email(String email, String mensagem) 
	{
		this.id = UUID.randomUUID().toString();
		this.destinatario = email;
		this.mensagem = mensagem;
	}
		
	public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public String getEmail() 
	{
		return destinatario;
	}
	
	public void setEmail(String email) 
	{
		this.destinatario = email;
	}
	
	public String getMensagem() 
	{
		return mensagem;
	}
	
	public void setMensagem(String mensagem) 
	{
		this.mensagem = mensagem;
	}
}
