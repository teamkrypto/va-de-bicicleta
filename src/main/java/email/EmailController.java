package email;

import java.util.LinkedHashMap;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import erro.Erro;
import io.javalin.http.Context;

public class EmailController {
	private EmailController() {

	}

	public static void enviaEmail(Context ctx) throws MessagingException {
		Properties prop = new Properties();
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.ssl.protocols", "TLSv1.2");
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.socketFactory.port", "465");
		prop.put("mail.smtp.port", "465");
		prop.put("mail.smtp.ssl.checkserveridentity", "true");
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getInstance(prop, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						"teamkryptounirio@gmail.com",
						"Bikepm123456");
			}
		});

		Message mensagem = montaEmail(session, ctx);

		Transport.send(mensagem);
		ctx.status(200);
	}

	public static EmailForm criaEmailForm(LinkedHashMap<String, String> request) {
		String email = request.get("email").toString();
		String mensagem = request.get("mensagem").toString();

		return new EmailForm(email, mensagem);
	}

	private static Message montaEmail(Session session, Context ctx) {
		Message mensagem = new MimeMessage(session);
		LinkedHashMap<String, String> request = ctx.bodyAsClass(LinkedHashMap.class);
		EmailForm emailJson = criaEmailForm(request);
		Email email = emailJson.desserializar();

		try {
			// De onde partem os e-mails (remetente)
			mensagem.setFrom(new InternetAddress("bicicleta.externo@gmail.com"));

			// Assunto do e-mail
			mensagem.setSubject("Sistema de Aluguel de Bicicletas");

			// Texto do e-mail
			mensagem.setText(email.getMensagem());

			// Destinatario
			mensagem.setRecipient(Message.RecipientType.TO, new InternetAddress(email.getEmail()));

			return mensagem;

		} catch (AddressException e) {
			ctx.status(404);
			Erro erro = new Erro();
			erro.setMensagem("Erro 404");
		} catch (MessagingException e) {
			ctx.status(422);
			Erro erro = new Erro();
			erro.setMensagem("Erro 422");
		}

		return null;
	}
}
