package main.util;

import static io.javalin.apibuilder.ApiBuilder.*;

import cartaodecredito.CartaoDeCreditoController;
import cobranca.CobrancaController;
import cobranca.FilaCobrancaController;
import email.EmailController;
import io.javalin.Javalin;

public class JavalinApp 
{
	private Javalin app =
			Javalin.create
			(
				config -> config.defaultContentType = "application/json")
            		.routes
            		(() -> 
	            		{
	            			path("/enviarEmail",  ()-> post(EmailController::enviaEmail));
	            			
	            			path("/cobranca",  ()-> post(CobrancaController::efetuaCobranca));
	            			
	            			path("/filaCobranca",  () -> post(FilaCobrancaController::postFilaCobranca));

	            			path("/cobranca/:id", ()-> get(CobrancaController::getCobranca));
	            			
	            			path("/validaCartaoDeCredito",  ()-> post(CartaoDeCreditoController::checkCartaoDeCredito));
	            		}
            		);
	
	public void start(int port) 
	{
        this.app.start(port);
    }

    public void stop() 
    {
        this.app.stop();
    }
}
