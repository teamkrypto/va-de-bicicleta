package cobranca;

import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;

import java.util.Objects;

import cartaodecredito.CartaoDeCredito;
import cartaodecredito.CartaoDeCreditoController;

public class CobrancaController {
	CobrancaController() {
		// construtor vazio
	}

	public static void getCobranca(Context ctx) {
		String cobrancaId = Objects.requireNonNull(ctx.pathParam("id"));

		CobrancaDAO cobrancaDAO = new CobrancaDAO();

		Cobranca cobranca = cobrancaDAO.getCobrancaById(cobrancaId);

		if (cobranca != null) {
			ctx.status(200);
			ctx.json(cobranca);

		} else {
			ctx.html("Cobrança não encontrada");
			ctx.status(404);
		}
	}

	public static void efetuaCobranca(Context ctx) {
		CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);

		Cobranca cobranca = cobrancaJson.desserializarPostCobranca();

		CartaoDeCreditoController cartaoController = new CartaoDeCreditoController();

		// Integração com método getCiclistaId do microsserviço Aluguel
		CartaoDeCredito cartao = cartaoController.getCartaoDeCreditoIntegracao(cobranca.getCiclistaId());

		ExpedicaoCobranca expedidora = new ExpedicaoCobranca(cobranca, cartao);

		HttpResponse<JsonNode> response = expedidora.enviaCobranca();

		ctx.json(response.getBody());

		if (response.getStatus() == 201) {
			ctx.status(201);
			return;
		} else {
			ctx.status(422);
		}
	}
}
