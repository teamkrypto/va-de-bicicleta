package cobranca;

import java.util.ArrayList;

public class CobrancaDAO 
{
	public CobrancaDAO()
	{
		// construtor vazio
	}
	
	public ArrayList<Cobranca> getCobrancas()
	{
		ArrayList<Cobranca> cobrancas = new ArrayList<Cobranca>();
		cobrancas.add(new Cobranca
				(
					"3fa85f64-5717-4562-b3fc-2c963f66afa6", 
					Status.PENDENTE, "2022-01-13T23:58:44.150Z", 
					"2022-01-13T23:58:44.150Z", 
					1, 
					"3fa85f64-5717-4562-b3fc-2c963f66afa8"
				));
		
		return cobrancas;
	}
	
	Cobranca getCobrancaById(String id)
	{
		ArrayList<Cobranca> cobrancas = getCobrancas();
		
		try {
			for (int i = 0; i < cobrancas.size(); i++) {
				if (cobrancas.get(i).getId().equals(id)) {
					return cobrancas.get(i);
				}
			}
		} catch (NullPointerException n) {
			return null;
		}
		
		return null;
	}
}
