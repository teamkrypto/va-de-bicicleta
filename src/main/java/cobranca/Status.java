package cobranca;

public enum Status 
{
	CANCELADA, OCUPADA, FALHA, PENDENTE, PAGA;
}
