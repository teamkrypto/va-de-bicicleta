package cobranca;


public class FilaCobrancaDAO 
{
	private FilaCobranca filaCobranca = new FilaCobranca();
	
  	public FilaCobranca getFilaCobrancaPreenchida() 
  	{
  		FilaCobranca filaCobrancas = new FilaCobranca();
  		
  		filaCobrancas.getFilaCobranca().add(new Cobranca
  				(	
					"61d10733-419e-45a5-845c-7fd5c71ec0c0",
  					Status.PENDENTE, 
  					"2022-01-14T20:33:00.491Z",
  					"2022-01-14T20:33:00.491Z", 
  					1, 
  					"3fa85f64-5717-4562-b3fc-2c963f66afa9"
  				));
  		
  		filaCobrancas.getFilaCobranca().add(new Cobranca
  				(	
					"3fa85f62-5717-4562-b3fc-2c963f66afa8", 
  					Status.CANCELADA, 
  					"2022-01-15T20:35:00.491Z", 
  					"", 
  					15, 
  					"124"
  				));
  		
  		filaCobrancas.getFilaCobranca().add(new Cobranca
  				(
					"dcb603f2-e318-4a82-90ec-dc3af850f06c", 
					Status.PAGA, 
					"2022-01-14T20:36:00.491Z",
  					"2022-01-14T20:37:00.491Z", 
					5, 
					"532"
				));
  		
  		return filaCobrancas;  		
  	}
  
  	public FilaCobranca getFilaCobranca() 
  	{
		return filaCobranca;
	}
}
