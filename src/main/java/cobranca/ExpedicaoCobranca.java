package cobranca;

import cartaodecredito.CartaoDeCredito;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class ExpedicaoCobranca {
	private Cobranca cobranca;
	private CartaoDeCredito cartao;

	private static final String MERCHANTID = "27e0f5ee-bd37-4efb-b74b-df15088c971a";
	private static final String MERCHANTKEY = "GPMUOJTNFTLUXPHQGIISPLJLUWATMOULQOPTABYU";
	private static final String REQUISICAO = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";

	public ExpedicaoCobranca() {

	}

	public ExpedicaoCobranca(Cobranca cobranca, CartaoDeCredito cartao) {
		this.cobranca = cobranca;
		this.cartao = cartao;
	}

	public Cobranca getCobranca() {
		return cobranca;
	}

	public void setCobranca(Cobranca cobranca) {
		this.cobranca = cobranca;
	}

	public HttpResponse<JsonNode> enviaCobranca() {
		CieloIntegracao transacao = new CieloIntegracao("1");

		transacao.Customer.Name = this.cartao.getNomeTitular();

		transacao.Payment.setAmount(cobranca.getValor());

		transacao.Payment.setInstallments(1);

		transacao.Payment.setType("CreditCard");

		transacao.Payment.getCreditCard().setBrand("MasterCard");

		transacao.Payment.getCreditCard().setCardNumber(cartao.getNumero());

		transacao.Payment.getCreditCard().setExpirationDate("12/2022");

		transacao.Payment.getCreditCard().setSecurityCode(cartao.getCvv());

		HttpResponse<JsonNode> response = Unirest.post(REQUISICAO)
				.header("Content-Type", "application/json")
				.header("MerchantKey", MERCHANTKEY)
				.header("MerchantId", MERCHANTID)
				.body(transacao)
				.asJson();
		return response;
	}

	public HttpResponse<JsonNode> validaCartaoDeCredito(CartaoDeCredito cartao) {
		CieloIntegracao transacao = new CieloIntegracao("1");

		transacao.Payment.setAmount(1);

		transacao.Payment.setInstallments(1);

		transacao.Payment.setType("CreditCard");

		transacao.Payment.getCreditCard().setBrand("Master");

		transacao.Payment.getCreditCard().setCardNumber(cartao.getNumero());

		transacao.Payment.getCreditCard().setExpirationDate(cartao.getValidade());

		transacao.Payment.getCreditCard().setSecurityCode(cartao.getCvv());

		HttpResponse<JsonNode> response = Unirest.post(REQUISICAO)
				.header("Content-Type", "application/json")
				.header("MerchantKey", MERCHANTKEY)
				.header("MerchantId", MERCHANTID)
				.body(transacao)
				.asJson();

		return response;
	}
}
