package cartaodecredito;

import io.javalin.http.Context;
import cobranca.ExpedicaoCobranca;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class CartaoDeCreditoController {

    private static final String URLINTEGRACAO = "https://teamkrypto-va-de-bike-aluguel.herokuapp.com/cartaoDeCredito/";

    public CartaoDeCreditoController() {
        // construtor
    }

    public static boolean checkCartaoDeCredito(Context ctx) {
        CartaoDeCredito cartao = ctx.bodyAsClass(CartaoDeCreditoForm.class).desserialize();
        ExpedicaoCobranca validacao = new ExpedicaoCobranca();
        HttpResponse<JsonNode> resultado = validacao.validaCartaoDeCredito(cartao);

        if (resultado.getStatus() == 201) {
            ctx.status(200);
            return true;
        } else if (resultado.getStatus() == 422) {
            ctx.status(422);
            return false;
        } else if (resultado.getStatus() == 402) {
            ctx.status(402);
            return false;
        }
        return false;
    }

    public CartaoDeCredito getCartaoDeCreditoIntegracao(String id) {
        HttpResponse<JsonNode> response = Unirest.get(URLINTEGRACAO + id).header("accept", "application/json").asJson();

        JSONObject json = response.getBody().getObject();

        CartaoDeCredito cartao = new CartaoDeCredito(
                json.getString("nomeTitular"),
                json.getString("numero"),
                json.getString("validade"),
                json.getString("cvv"));

        return cartao;
    }

    public CartaoDeCredito getCartaoFake() {
        CartaoDeCredito cartao = new CartaoDeCredito(
                "Fulano Teste",
                "0000.0000.0000.0004",
                "12/2024",
                "321");

        return cartao;
    }

    public static boolean validaCartaoFake(long number) {
        return (getSize(number) >= 13 &&
                getSize(number) <= 16) &&
                (prefixMatched(number, 4) ||
                        prefixMatched(number, 5) ||
                        prefixMatched(number, 37) ||
                        prefixMatched(number, 6))
                &&
                ((sumOfDoubleEvenPlace(number) + sumOfOddPlace(number)) % 10 == 0);
    }

    public static boolean prefixMatched(long number, int d) {
        return getPrefix(number, getSize(d)) == d;
    }

    public static int sumOfDoubleEvenPlace(long number) {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 2; i >= 0; i -= 2)
            sum += getDigit(Integer.parseInt(num.charAt(i) + "") * 2);

        return sum;
    }

    public static int getDigit(int number) {
        if (number < 9)
            return number;
        return number / 10 + number % 10;
    }

    public static int sumOfOddPlace(long number) {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 1; i >= 0; i -= 2)
            sum += Integer.parseInt(num.charAt(i) + "");
        return sum;
    }

    public static int getSize(long d) {
        String num = d + "";
        return num.length();
    }

    public static long getPrefix(long number, int k) {
        if (getSize(number) > k) {
            String num = number + "";
            return Long.parseLong(num.substring(0, k));
        }
        return number;
    }
}
